import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by krzysztofgrys on 20/10/2016.
 */
public class Zad4 {

    public Zad4() {

    }


    public void draw() {
        Helpers helpers = new Helpers();

        BufferedImage image1 = null;
        BufferedImage image2 = null;
        BufferedImage result;

        int img_x, img_y;

        double a = Double.parseDouble("0.5");

        try {
            image1 = ImageIO.read(new File("obraz2.jpg"));
            image2 = ImageIO.read(new File("obraz1.jpg"));

        } catch (IOException e) {

        }

        if (image1.getHeight() != image2.getHeight() && image1.getWidth() != image2.getWidth()) {
            if (image1.getHeight() > image2.getHeight() || image1.getWidth() > image2.getWidth()) {
                image1 = helpers.resizeImage(image1, BufferedImage.TYPE_INT_BGR, image2.getHeight(), image2.getWidth());
            } else {
                image2 = helpers.resizeImage(image2, BufferedImage.TYPE_INT_BGR, image1.getHeight(), image1.getWidth());
            }
        }

        img_x = image1.getHeight();
        img_y = image1.getWidth();

        result = new BufferedImage(img_y, img_x, BufferedImage.TYPE_INT_RGB);

        for (int i = 0; i < img_y; i++) {
            for (int j = 0; j < img_x; j++) {
                int rgb1[] = helpers.RGBToInt(image1.getRGB(i, j));
                int rgb2[] = helpers.RGBToInt(image2.getRGB(i, j));

                Double r = a * rgb1[0] + (1 - a) * rgb2[0];
                Double g = a * rgb1[1] + (1 - a) * rgb2[1];
                Double b = a * rgb1[2] + (1 - a) * rgb2[2];


                result.setRGB(i, j, helpers.int2RGB(r.intValue(), g.intValue(), b.intValue()));

            }
        }


        String path = Paths.get(".").toAbsolutePath().normalize().toString();


        try {
            ImageIO.write(result, "bmp", new File(path + "/wyniki/zad4-zajecia.bmp"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }

    }

    public void drawProc() {

        Helpers helpers = new Helpers();

        BufferedImage image1 = null;
        BufferedImage image2 = null;
        BufferedImage result;

        int img_x, img_y;

        double a = Double.parseDouble("0.5");

        try {
            image1 = ImageIO.read(new File("obraz2.jpg"));
            image2 = ImageIO.read(new File("obraz1.jpg"));

        } catch (IOException e) {

        }

        if (image1.getHeight() != image2.getHeight() && image1.getWidth() != image2.getWidth()) {
            if (image1.getHeight() > image2.getHeight() || image1.getWidth() > image2.getWidth()) {
                image1 = helpers.resizeImage(image1, BufferedImage.TYPE_INT_BGR, image2.getHeight(), image2.getWidth());
            } else {
                image2 = helpers.resizeImage(image2, BufferedImage.TYPE_INT_BGR, image1.getHeight(), image1.getWidth());
            }
        }

        img_x = image1.getHeight();
        img_y = image1.getWidth();

        ;

        result = new BufferedImage(img_y, img_x, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < img_y; y++) {
            for (int x = 0; x < img_x; x++) {
                result.setRGB(x, y, image2.getRGB(x, y));
            }

        }


        for (int y = 0; y < img_y; ) {
            for (int x = 0; x < img_x; ) {

                draw(x, y, img_x, img_y, result, image1, image2, helpers);
                x += 50;
            }
            y += 50;
        }


        String path = Paths.get(".").toAbsolutePath().normalize().toString();


        try {
            ImageIO.write(result, "bmp", new File(path + "/wyniki/zad4"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }
    }


    private void draw(int x, int y, int img_x, int img_y, BufferedImage result, BufferedImage image1, BufferedImage image2, Helpers helpers) {
        for (int y1 = 0; y1 < 50; y1++) {
            for (int x1 = 0; x1 < 50; x1++) {
                if (x1 <= 25 && y1 <= 25) {
                    if (y1 >= x1 * (-1) + 25 && x1 >= y1 * (-1) + 25) {
                        if (x1 + x < img_x && y + y1 < img_y) {
                            result.setRGB(x1 + x, y + y1, image1.getRGB(x1 + x, y + y1));
                        }
                    } else {
                        if (x1 + x < img_x && y + y1 < img_y) {
                            result.setRGB(x1 + x, y + y1, image2.getRGB(x1 + x, y + y1));
                        }
                    }
                } else {
                    if (x1 < 25 && y1 > 25) {
                        if (y1 > x1 + 25 && x1 > y1 * (-1) - 25) {
                            if (x1 + x < img_x && y + y1 < img_y) {
                                result.setRGB(x1 + x, y + y1, image2.getRGB(x1 + x, y + y1));
                            }
                        } else {
                            if (x1 + x < img_x && y + y1 < img_y) {
                                result.setRGB(x1 + x, y + y1, image1.getRGB(x1 + x, y + y1));
                            }

                        }
                    }

                    if (x1 >= 25 && y1 <= 25) {

                        if (y1 > -x1 + 25 && x1 > y1 + 25) {
                            if (x1 + x < img_x && y + y1 < img_y) {
                                result.setRGB(x1 + x, y + y1, image2.getRGB(x1 + x, y + y1));
                            }
                        } else {
                            if (x1 + x < img_x && y + y1 < img_y) {
                                result.setRGB(x1 + x, y + y1, image1.getRGB(x1 + x, y + y1));
                            }
                        }
                    } else {
                        if (x1 >= 25 && y1 >= 25) {

                            if (y1 > x1 * (-1) + 75 && x1 > -y1 + 75) {
                                if (x1 + x < img_x && y + y1 < img_y) {
                                    result.setRGB(x1 + x, y + y1, image2.getRGB(x1 + x, y + y1));
                                }
                            } else {
                                if (x1 + x < img_x && y + y1 < img_y) {
                                    result.setRGB(x1 + x, y + y1, image1.getRGB(x1 + x, y + y1));
                                }
                            }
                        }
                    }

                }
            }
        }

    }
}
