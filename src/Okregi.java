
import java.io.*;
import java.awt.image.*;
import javax.imageio.*;
import java.nio.file.Paths;
import java.sql.BatchUpdateException;
import java.util.Scanner;


public class Okregi {

    private int img_x;
    private int img_y;
    private int height;

    public Okregi() {
    }


    public void draw() {
        BufferedImage image1 = null;
        BufferedImage image2 = null;
        BufferedImage result = null;


        Helpers helpers = new Helpers();

        int x_c, y_c;
        int i, j;

        Double[] szarosc = new Double[201];
        Double ads =0.01;
        for(i=0;i<100;i++){
            szarosc[i] = ads;
            ads+=0.01;
        }
        ads = 1.00;
        for(i=100;i<200;i++){
            szarosc[i] = ads;
            ads-=0.01;
        }


        final int w = 1;
        int ile = 0;

        try {
            image1 = ImageIO.read(new File("2.jpg"));
            image2 = ImageIO.read(new File("1.jpg"));

        } catch (IOException e) {

        }

        img_x = image1.getHeight();
        img_y = image1.getWidth();

        result = new BufferedImage(img_x, img_y, BufferedImage.TYPE_INT_RGB);

        x_c = img_x / 2;
        y_c = img_y / 2;

        for (i = 0; i < img_y; i++) {
            for (j = 0; j < img_x; j++) {
                double d;
                int rr;

                d = Math.sqrt((i - y_c) * (i - x_c) + (j - x_c) * (j - x_c));

                rr = (int) d / w;

                int rgb1[] = helpers.RGBToInt(image1.getRGB(i, j));
                int rgb2[] = helpers.RGBToInt(image2.getRGB(i, j));

                Double a = szarosc[rr%200];

                Double r = a * rgb1[0] + (1 - a) * rgb2[0];
                Double g = a * rgb1[1] + (1 - a) * rgb2[1];
                Double b = a * rgb1[2] + (1 - a) * rgb2[2];


                result.setRGB(i, j, helpers.int2RGB(r.intValue(), g.intValue(), b.intValue()));

            }
        }


        String path = Paths.get(".").toAbsolutePath().normalize().toString();


        try {
            ImageIO.write(result, "bmp", new File(path + "/wyniki/nalozenie.bmp"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }


    }

    public void gradient(){

        BufferedImage image1 = null;
        BufferedImage image2 = null;
        BufferedImage result = null;

        try {
            image1 = ImageIO.read(new File("2.jpg"));
            image2 = ImageIO.read(new File("1.jpg"));

        } catch (IOException e) {

        }

        img_x = image1.getHeight();
        img_y = image1.getWidth();

        result = new BufferedImage(img_x, img_y, BufferedImage.TYPE_INT_RGB);
        Double a=1.000;
        Helpers helpers = new Helpers();
        for(int y=0;y<img_y;y++){
            for(int x=0;x<img_x;x++){

                int rgb1[] = helpers.RGBToInt(image1.getRGB(x, y));
                int rgb2[] = helpers.RGBToInt(image2.getRGB(x, y));


                Double r = a * rgb1[0] + (1 - a) * rgb2[0];
                Double g = a * rgb1[1] + (1 - a) * rgb2[1];
                Double b = a * rgb1[2] + (1 - a) * rgb2[2];


                result.setRGB(x, y, helpers.int2RGB(r.intValue(), g.intValue(), b.intValue()));
                a-=0.001;

            }
            a=1.000;
        }


        String path = Paths.get(".").toAbsolutePath().normalize().toString();


        try {
            ImageIO.write(result, "bmp", new File(path + "/wyniki/nalozenieGradient.bmp"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }


    }
}
