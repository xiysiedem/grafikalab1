import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by krzysztofgrys on 18/10/2016.
 */
public class Wzor4 {

    private int img_x;
    private int img_y;
    private int black;
    private int white;

    public Wzor4() {

    }


    public void set() {
        Helpers helpers = new Helpers();
        BufferedImage image = null;


        img_x = 1000;
        img_y = 1000;

        black = helpers.int2RGB(0, 0, 0);
        white = helpers.int2RGB(255, 255, 255);

        image = new BufferedImage(img_x, img_y, BufferedImage.TYPE_INT_RGB);
        helpers.set_bg_color(image, img_x, img_y, white);

        for (int y = 0; y < img_y; ) {
            for (int x = 0; x < img_x; ) {
                draw(x, y, img_x, img_y, image);
                x += 100;
            }
            y += 100;
        }


        String path = Paths.get(".").toAbsolutePath().normalize().toString();

        try {
            System.out.println("Zapisano w " + path);
            ImageIO.write(image, "bmp", new File(path + "/wyniki/wzor4.bmp"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }

    }

    private void draw(int x, int y, int img_x, int img_y, BufferedImage image) {
        for (int y1 = 0; y1 < 100; y1++) {
            for (int x1 = 0; x1 < 100; x1++) {
                double d;
                int r;

                d = Math.sqrt((y1 - 50) * (y1 - 56) + (x1 - 56) * (x1 - 56));
//                    System.out.println(d);
                r = (int) d / 6;

                if (r % 2 == 0) {
                    image.setRGB(y1 + y, x1 + x, black);
                } else {
                    image.setRGB(y1 + y, x1 + x, white);
                }
            }
        }
    }


}
