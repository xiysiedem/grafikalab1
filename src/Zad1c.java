import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Created by krzysztofgrys on 19/10/2016.
 */
public class Zad1c {

    private int img_x;
    private int img_y;
    private int pole1;
    private int pole2;
    private int wielkosc;
    private int linecolor;
    private int bg_color;

    public Zad1c() {

    }

    public void parameters() {
        Helpers helpers = new Helpers();
        boolean correct = false;
        while (!correct) {
            Scanner input = new Scanner(System.in);

            System.out.println("Podaj parametry: ");
            System.out.print("rozmiar x: ");
            img_x = input.nextInt();

            System.out.print("rozmiar y: ");
            img_y = input.nextInt();

            System.out.println("wielkosc pola: ");
            wielkosc = input.nextInt();

            System.out.println("kolor szachownicy 1: ");
            System.out.print("R: ");
            int r = input.nextInt();

            System.out.print("G: ");
            int g = input.nextInt();

            System.out.print("B: ");
            int b = input.nextInt();

            pole1 = helpers.int2RGB(r,g,b);

            System.out.println("kolor szachownicy 2: ");
            System.out.print("R: ");
            r = input.nextInt();

            System.out.print("G: ");
            g = input.nextInt();

            System.out.print("B: ");
            b = input.nextInt();

            pole2 = helpers.int2RGB(r,g,b);

            if (img_x > 0 && img_y > 0) {
                correct = true;
                draw();
            }else{
                System.out.println("Wprowadziles zle parametry");
            }

        }


    }
    public void draw() {
        Helpers helpers = new Helpers();
        BufferedImage image = null;


        image = new BufferedImage(img_x, img_y, BufferedImage.TYPE_INT_RGB);
        int ile=0;
        for(int y=0;y<img_y;y+=wielkosc) {
            for (int x = 0; x < img_x;) {
                if(ile%2==0){
                    draw_pole(x, y, pole1, img_x, img_y, image, wielkosc);
                    x += wielkosc;
                    draw_pole(x, y, pole2, img_x, img_y, image, wielkosc);
                    x += wielkosc;
                }else{
                    draw_pole(x, y, pole2, img_x, img_y, image, wielkosc);
                    x += wielkosc;
                    draw_pole(x, y, pole1, img_x, img_y, image, wielkosc);
                    x += wielkosc;
                }

            }
            ile++;

        }


        String path = Paths.get(".").toAbsolutePath().normalize().toString();

        try {
            ImageIO.write(image, "bmp", new File(path + "/wyniki/zad1c.bmp"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }

    }


    public void xd(){
        Helpers helpers = new Helpers();
        BufferedImage image = null;


        pole1 = helpers.int2RGB(255, 120, 255);
        pole2 = helpers.int2RGB(0, 120, 0);


        img_x = 1000;
        img_y = 1000;
        wielkosc = 50;



        image = new BufferedImage(img_x, img_y, BufferedImage.TYPE_INT_RGB);


        for(int x=0;x<img_x;){
                draw_45(x, 0, pole1, img_x, img_y, image, wielkosc);
                x+=wielkosc+10;


        }


        String path = Paths.get(".").toAbsolutePath().normalize().toString();

        try {
            ImageIO.write(image, "bmp", new File(path + "/wyniki/zad1c.bmp"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }


    }


    private void draw_45(int start_x, int start_y, int color, int img_x, int img_y, BufferedImage image, int wielkosc){
        int y = start_y;
        int x = start_x;
        int krok =1;

        while(krok<wielkosc){

        }



    }
    private void draw_pole(int start_x, int start_y, int color, int img_x, int img_y, BufferedImage image, int wielkosc){
        int y = start_y;
        int x = start_x;

      while(y<wielkosc+start_y && y<img_y){
          x=start_x;
          while(x<wielkosc+start_x){
              if(x<img_x){
                  image.setRGB(x,y,color);
              }
              x++;
          }
          y++;
      }

    }


}
