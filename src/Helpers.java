import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by krzysztofgrys on 18/10/2016.
 */
public class Helpers {


    public int int2RGB(int red, int green, int blue) {
        red = red & 0x000000FF;
        green = green & 0x000000FF;
        blue = blue & 0x000000FF;

        return (red << 16) + (green << 8) + blue;
    }

    public void set_bg_color(BufferedImage image, int x, int y, int color) {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                image.setRGB(i, j, color);
            }
        }
    }
    public int[] RGBToInt(int rgb) {
        int argb[] = new int[]{
                (rgb >> 16) & 0xff,
                (rgb >> 8) & 0xff,
                (rgb) & 0xff
        };

        return argb;
    }

    public static BufferedImage resizeImage(BufferedImage originalImage, int type, int img_x, int img_y) {
        BufferedImage resizedImage = new BufferedImage(img_x, img_y, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, img_x, img_y, null);
        g.dispose();

        return resizedImage;
    }
}
