
import java.io.*;
import java.awt.image.*;
import javax.imageio.*;
import java.nio.file.Paths;
import java.util.Scanner;


public class Zad1a {

    private int img_x;
    private int img_y;
    private int height;

    public Zad1a() {
    }


    public void parameters() {
        boolean correct = false;
        while (!correct) {
            Scanner input = new Scanner(System.in);

            System.out.println("Podaj parametry: ");
            System.out.print("rozmiar x: ");
            img_x = input.nextInt();

            System.out.print("rozmiar y: ");
            img_y = input.nextInt();

            System.out.print("szerokość strefy rozmycia(max 255) ");
            height = input.nextInt();

            if (img_x > 0 && img_y > 0 && height > 0) {
                correct = true;
                draw();
            }

        }

    }

    public void draw() {

        BufferedImage image;

        int x_c, y_c;
        int i, j;

        final int w = 1;
        int ile = 0;



        image = new BufferedImage(img_x, img_y, BufferedImage.TYPE_INT_RGB);

        Double[] szarosc = new Double[100];
        Double ads =0.01;
        for(i=0;i<100;i++){
            szarosc[i] = ads;
            ads+=0.01;
        }



        x_c = img_x / 2;
        y_c = img_y / 2;

        for (i = 0; i < img_y; i++) {
            for (j = 0; j < img_x; j++) {
                double d;
                int r;

                d = Math.sqrt((i - y_c) * (i - x_c) + (j - x_c) * (j - x_c));
                r = (int) d / w;


//                image.setRGB(i, j, szarosc[r%100]);

            }
        }


        String path = Paths.get(".").toAbsolutePath().normalize().toString();


        try {
            ImageIO.write(image, "bmp", new File(path + "/wyniki/zad1a.bmp"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }


    }


    static int int2RGB(int red, int green, int blue) {
        red = red & 0x000000FF;
        green = green & 0x000000FF;
        blue = blue & 0x000000FF;

        return (red << 16) + (green << 8) + blue;
    }
}
