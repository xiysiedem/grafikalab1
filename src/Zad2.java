import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by krzysztofgrys on 18/10/2016.
 */
public class Zad2 {

    private int img_x;
    private int img_y;
    private int line_h;
    private int odl_x;
    private int odl_y;
    private int linecolor;
    private int bg_color;

    public Zad2() {

    }


    public void set() {
        Helpers helpers = new Helpers();
        BufferedImage image = null;
        BufferedImage result = null;


        line_h = 10;
        odl_x = 50;
        odl_y =50;


        try {
            image = ImageIO.read(new File("obraz2.jpg"));

        } catch (IOException e) {
            System.out.println("NIE MA");
        }

        img_x = image.getWidth();
        img_y = image.getHeight();

        System.out.println(img_x+" "+img_y);

        linecolor = helpers.int2RGB(0, 0, 0);
        bg_color = helpers.int2RGB(255, 255, 255);

        result = new BufferedImage(img_x, img_y, BufferedImage.TYPE_INT_RGB);

        draw(image,result, img_x, img_y, odl_x, 1, helpers);

        draw(image,result, img_x, img_y, odl_y, 0, helpers);


        String path = Paths.get(".").toAbsolutePath().normalize().toString();

        try {
            System.out.println("Zapisano w " + path);
            ImageIO.write(result, "bmp", new File(path+"/wyniki/zad2.bmp"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }

    }

    private void draw(BufferedImage image, BufferedImage result, int img_x, int img_y, int odl, int a, Helpers helpers) {


        for (int y = 0; y < img_y; y++) {
            for (int x = 0; x < img_x; x++) {
                if (x % odl - (line_h / 2) == 0) {
                    int tmp = 0;
                    while (tmp < line_h && (x < img_x && y< img_y)) {
                        if (a == 0) {
                            result.setRGB(y, x, linecolor);

                        } else {
                            result.setRGB(x, y, linecolor);

                        }
                        tmp++;
                        if (x < img_x) {
                            x++;
                        }
                    }
                }
                else{
                    if(a == 1) {
                        result.setRGB(x, y, image.getRGB(x,y));
                    }

                }
            }
        }
    }


}
