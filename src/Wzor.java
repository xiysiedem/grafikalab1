import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by krzysztofgrys on 18/10/2016.
 */
public class Wzor {

    private int img_x;
    private int img_y;
    private int line_h;
    private int odl_x;
    private int odl_y;
    private int black;
    private int white;

    public Wzor() {

    }


    public void set() {
        Helpers helpers = new Helpers();
        BufferedImage image = null;


        img_x = 1000;
        img_y = 1000;

        black = helpers.int2RGB(0, 0, 0);
        white = helpers.int2RGB(255, 255, 255);

        image = new BufferedImage(img_x, img_y, BufferedImage.TYPE_INT_RGB);
        int[] srodek = {img_x/2, img_y/2};

        for(int y=0;y<img_y;y++){
            for(int x=0;x<img_x;x++){
                Double d = Math.sqrt(((y-srodek[0])*(y-srodek[1]) + (x-srodek[0])*(x-srodek[1])));
                Double kat = Math.toDegrees(Math.asin(Math.abs(y-srodek[1])/d));

                if(x<srodek[0] && y>srodek[1]){
                    kat+=90;
                }else{
                    if(x<srodek[0] && y<srodek[1]){
                        kat+=180;
                    }else{
                        if(x>srodek[0] && y<srodek[1]){
                            kat+=270;
                        }
                    }
                }



                if(Math.toDegrees(kat)%16<8){
                    image.setRGB(x,y,black);
                }else{
                    image.setRGB(x,y,white);
                }

            }
        }



        String path = Paths.get(".").toAbsolutePath().normalize().toString();

        try {
            System.out.println("Zapisano w " + path);
            ImageIO.write(image, "bmp", new File(path+"/asdq.bmp"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }

    }


}
