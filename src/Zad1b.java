import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Created by krzysztofgrys on 18/10/2016.
 */
public class Zad1b {

    private int img_x;
    private int img_y;
    private int line_h;
    private int odl_x;
    private int odl_y;
    private int linecolor;
    private int bg_color;

    public Zad1b() {

    }

    public void parameters() {
        Helpers helpers = new Helpers();
        boolean correct = false;
        while (!correct) {
            Scanner input = new Scanner(System.in);

            System.out.println("Podaj parametry: ");
            System.out.print("rozmiar x: ");
            img_x = input.nextInt();

            System.out.print("rozmiar y: ");
            img_y = input.nextInt();

            System.out.print("Grubosc lini: ");
            line_h = input.nextInt();

            System.out.print("Odleglosc od srodka linia x: ");
            odl_x = input.nextInt();

            System.out.print("Odleglosc od srodka linia y: ");
            odl_y = input.nextInt();

            System.out.print("kolor tla: ");
            System.out.print("R: ");
            int r = input.nextInt();

            System.out.print("G: ");
            int g = input.nextInt();

            System.out.print("B: ");
            int b = input.nextInt();

            bg_color = helpers.int2RGB(r,g,b);

            System.out.print("kolor lini: ");
            System.out.print("R: ");
             r = input.nextInt();

            System.out.print("G: ");
             g = input.nextInt();

            System.out.print("B: ");
             b = input.nextInt();

            linecolor = helpers.int2RGB(r,g,b);

            if (img_x > 0 && img_y > 0 && odl_x > 0 && odl_y > 0 && bg_color >= 0 && linecolor >= 0) {
                correct = true;
                draw();
            }else{
                System.out.println("Wprowadziles zle parametry");
            }

        }


    }

    public void draw() {
        Helpers helpers = new Helpers();
        BufferedImage image = null;



        while (odl_x < line_h / 2 || odl_y < line_h / 2) {
            System.out.println("Grubosc lini nie moze byc mniejsza od srodka");
        }


        img_x = 1000;
        img_y = 1000;

        linecolor = helpers.int2RGB(0, 0, 0);
        bg_color = helpers.int2RGB(255, 255, 255);

        image = new BufferedImage(img_x, img_y, BufferedImage.TYPE_INT_RGB);

        helpers.set_bg_color(image, img_x, img_y, bg_color);


        draw(image, img_y, img_x, odl_x, 1);

        draw(image, img_x, img_y, odl_y, 0);


        String path = Paths.get(".").toAbsolutePath().normalize().toString();

        try {
            ImageIO.write(image, "bmp", new File(path + "/wyniki/zad1b.bmp"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }

    }


    private void draw(BufferedImage image, int img_x, int img_y, int odl, int a) {

        for (int y = 0; y < img_y; y++) {
            for (int x = 0; x < img_x; x++) {
                if (x % odl - (line_h / 2) == 0) {
                    int tmp = 0;
                    while (tmp < line_h && x < img_x) {
                        if (a == 0) {
                            image.setRGB(y, x, linecolor);

                        } else {
                            image.setRGB(x, y, linecolor);

                        }
                        tmp++;
                        if (x < img_x) {
                            x++;
                        }
                    }
                }
            }
        }
    }


}
