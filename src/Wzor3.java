import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by krzysztofgrys on 18/10/2016.
 */
public class Wzor3 {

    private int img_x;
    private int img_y;
    private int black;
    private int white;

    public Wzor3() {

    }


    public void set() {
        Helpers helpers = new Helpers();
        BufferedImage image = null;


        img_x = 1000;
        img_y = 1000;

        black = helpers.int2RGB(0, 0, 0);
        white = helpers.int2RGB(255, 255, 255);

        image = new BufferedImage(img_x, img_y, BufferedImage.TYPE_INT_RGB);
        helpers.set_bg_color(image, img_x, img_y, white);

        int[] srodek = {img_x / 2, img_y / 2};
        Double modulo = 6.0;
        for (int y = 0; y < img_y; y++) {
            for (int x = 0; x < img_x; x++) {
                Double d;
                Double r;
                d = Math.sqrt((y - srodek[0]) * (y - srodek[0]) + (x - srodek[1]) * (x - srodek[1]));

                r = d.intValue() / modulo;

                if (r % 2 ==0) {
                    image.setRGB(y, x, black);

                } else {
                    image.setRGB(y, x, white);
                }


            }
//            modulo-=0.01;
        }


        String path = Paths.get(".").toAbsolutePath().normalize().toString();

        try {
            System.out.println("Zapisano w " + path);
            ImageIO.write(image, "bmp", new File(path + "/wyniki/wzor3.bmp"));

        } catch (IOException e) {
            System.out.println("Can't save image");
        }

    }

    private void draw(int x, int y, int img_x, int img_y, BufferedImage image) {

    }


}
